﻿using System;
using System.Runtime.Serialization;

namespace ChaosProject
{
    [Serializable]
    internal class IndexNotAvailable : Exception
    {
        public IndexNotAvailable(string message) : base(message)
        {
        }
    }
}