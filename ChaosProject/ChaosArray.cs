﻿using Microsoft.VisualBasic.CompilerServices;
using System;

namespace ChaosProject
{
    public class ChaosArray<T>
    {
        private object[] array;
        private Random random;

        public ChaosArray(int size = 10)
        {
            array = new object[size];
            random = new Random();
        }

        public void Insert(T item)
        {
            int index = random.Next(0, array.Length - 1);

            if (array[index] != default)
            {
                throw new IndexNotAvailable($"Index { index } in the collection is already in use");
            }
            else
            {
                array[index] = item;
            }
        }

        public T Retrieve()
        {
            int index = random.Next(0, array.Length - 1);

            if (array[index] == default)
            {
                throw new CannotFindItemAtIndex($"Index { index } is outside the bounds of the collection");
            }
            else
            {
                T temp = (T)array[index];
                array[index] = default;
                return temp;
            }
        }

        public void Print()
        {
            foreach (var item in array)
            {
                if (item == null)
                {
                    Console.WriteLine("Null");
                }
                else
                {
                    Console.WriteLine(item);
                }
            }
        }
    }
}
