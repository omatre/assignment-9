﻿using System;

namespace ChaosProject
{
    class Program
    {
        static void Main(string[] args)
        {
            var array = new ChaosArray<int>();
            var random = new Random();

            for (int i = 0; i < 10; i++)
            {
                try
                {
                    array.Insert(random.Next(0, 100));
                }
                catch (CannotFindItemAtIndex ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (IndexNotAvailable ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            for (int i = 0; i < 10; i++)
            {
                try
                {
                    Console.WriteLine($"Retrieved: { array.Retrieve() }");
                }
                catch (CannotFindItemAtIndex ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (IndexNotAvailable ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            array.Print();
        }
    }
}
