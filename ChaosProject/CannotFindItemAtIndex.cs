﻿using System;
using System.Runtime.Serialization;

namespace ChaosProject
{
    [Serializable]
    internal class CannotFindItemAtIndex : Exception
    {
        public CannotFindItemAtIndex(string message) : base(message)
        {
        }
    }
}